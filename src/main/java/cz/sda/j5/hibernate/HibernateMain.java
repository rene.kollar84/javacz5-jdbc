package cz.sda.j5.hibernate;

import cz.sda.j5.hibernate.entity.*;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
public class HibernateMain {
    public static void main(String[] args) {

        try (SessionFactory sessionFactory = getHibernateSession()) {
            try (Session session = sessionFactory.openSession()) {
                Transaction transaction = session.getTransaction();
                transaction.begin();
                //create product
                Product whirpoool_a1233 =
                        new Product("Whirpoool a1233",
                                new BigDecimal(10000),
                                CategoryEnum.WASHMACHINE,
                                new Manufacturer("Whirpool","Japon"));
                session.save(whirpoool_a1233);


                Query<Product> from_product = session.createQuery("FROM Product", Product.class);
                List<Product> list = from_product.list();
                list.forEach(System.out::println);


                Customer customer = new Customer("Jan Novak",
                        LocalDate.of(1984,5,28), Set.of());
                session.save(customer);
                session.createQuery("FROM Customer",Customer.class)
                        .list().forEach(System.out::println);


                //create order
                Order order = new Order();
                order.setOrderDate(LocalDate.now());
                order.setCustomer(customer);
                order.setItemList(new HashSet<>());
                session.save(order);
                order.getItemList().add(new OrderItem(whirpoool_a1233,order));
                session.save(order);


                List<Order> from_order = session.createQuery("FROM Order", Order.class).list();
                transaction.commit();
            } catch (Exception e) {
                log.error("Chyba v mainu open session", e);
            }

        } catch (Exception e) {
            log.error("Chyba v mainu", e);
        }


    }

    private static SessionFactory getHibernateSession() {
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();
        Metadata metadata = new MetadataSources(serviceRegistry)
                .addAnnotatedClass(Customer.class)
                .addAnnotatedClass(Order.class)
                .addAnnotatedClass(OrderItem.class)
                .addAnnotatedClass(Product.class)
                .getMetadataBuilder()
                .applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE)
                .build();

        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
        return sessionFactory;
    }
}
