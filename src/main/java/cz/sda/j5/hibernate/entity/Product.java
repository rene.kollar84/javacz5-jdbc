package cz.sda.j5.hibernate.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
//@Table(name="produkt_v_cestine")
public class Product extends BaseEntity {


    @Column
    private String name;

    @Column(name = "cena")
    private BigDecimal price;

    @Column
    @Enumerated(EnumType.STRING)
    private CategoryEnum category;

    @Embedded
    private Manufacturer manufacturer;
}
