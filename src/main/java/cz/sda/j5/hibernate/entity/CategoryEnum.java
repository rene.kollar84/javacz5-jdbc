package cz.sda.j5.hibernate.entity;

public enum CategoryEnum {
    FRIDGE,
    WASHMACHINE,
    PC,
    TV,
    AUDIO,
    COMPUTERS
    // TODO fill other categories
}
