package cz.sda.j5.hibernate.repository;

import cz.sda.j5.hibernate.entity.CategoryEnum;
import cz.sda.j5.hibernate.entity.Product;
import org.hibernate.Session;


import java.math.BigDecimal;
import java.util.List;

public class ProductRepositoryImpl extends BaseCrud<Product> implements ProductRepository {
    @Override
    public Product createProduct(String name, BigDecimal price, CategoryEnum categoryEnum) {
        Product item = new Product();
        item.setName(name);
        item.setPrice(price);
        item.setCategory(categoryEnum);
        return saveObject(item);
    }

    @Override
    public List<Product> getAll() {
        return getAll(Product.class);
    }

    @Override
    public List<Product> getAllBy(CategoryEnum... category) {
        return getAllByQuery("FROM Product WHERE category=?1", Product.class, category);
    }

    @Override
    public List<Product> getAllByPricesInterval(int from, int to) {
        return getAllByQuery("FROM Product WHERE price >= ?1 AND price <= ?2", Product.class, from, to);
    }

    @Override
    public BigDecimal getSumPriceForCategory(CategoryEnum... cat) {
       return (BigDecimal) getSingleObject("SELECT SUM(price) FROM Product WHERE category=?1",Product.class,cat);
    }

    @Override
    public String getProductTitleById(int id) {
        return (String) getSingleObject("SELECT name FROM Product WHERE id=?1",Product.class,id);
    }

    public ProductRepositoryImpl(Session entityManager) {
        super(entityManager);
    }
}
