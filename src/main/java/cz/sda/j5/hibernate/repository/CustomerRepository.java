package cz.sda.j5.hibernate.repository;

import cz.sda.j5.hibernate.entity.Customer;
import jakarta.persistence.Column;

import java.util.List;

public interface CustomerRepository {
    //@Query("FROM Customer WHERE ....")
    List<Customer> getCustumerByAddress();
}
