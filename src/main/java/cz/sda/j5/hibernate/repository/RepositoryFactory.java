package cz.sda.j5.hibernate.repository;

import org.hibernate.Session;

public class RepositoryFactory {
    public static ProductRepository createProductRepo(Session entityManager) {
        return new ProductRepositoryImpl(entityManager);

    }
}
