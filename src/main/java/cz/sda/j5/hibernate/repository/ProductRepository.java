package cz.sda.j5.hibernate.repository;

import cz.sda.j5.hibernate.entity.CategoryEnum;
import cz.sda.j5.hibernate.entity.Product;


import java.math.BigDecimal;
import java.util.List;

public interface ProductRepository {

    Product createProduct(String name, BigDecimal price, CategoryEnum categoryEnum);

    List<Product> getAll();

    List<Product> getAllBy(CategoryEnum ... category);

    List<Product> getAllByPricesInterval(int from,int to);

    BigDecimal getSumPriceForCategory(CategoryEnum ... cat);

    String getProductTitleById(int id);
}
