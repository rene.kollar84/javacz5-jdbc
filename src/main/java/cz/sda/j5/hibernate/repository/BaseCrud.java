package cz.sda.j5.hibernate.repository;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class BaseCrud<T> {
    Session entityManager;

    public BaseCrud(Session entityManager) {
        this.entityManager = entityManager;
    }

    public T saveObject(T object) {
        entityManager.persist(object);
        return object;
    }

    public List<T> getAll(Class<T> clazz) {
        return entityManager.createQuery("FROM " + clazz.getSimpleName()).list();
    }

    public List<T> getAllByQuery(String query,Class<T> clazz, Object ... param) {
        Query<T> ret = (Query<T>) entityManager.createQuery(query, clazz);
        for (int i = 0; i < param.length; i++) {
            ret.setParameter(i+1,param[i]);
        }
        return ret.list();
    }

    public Object getSingleObject(String query, Class<T> clazz, Object ... param) {
        Query ret = entityManager.createQuery(query, clazz);
        for (int i = 0; i < param.length; i++) {
            ret.setParameter(i+1,param[i]);
        }
        return  ret.getSingleResult();
    }
}
