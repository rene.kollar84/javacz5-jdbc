package cz.sda.j5.jdbc.task2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MovieDAOImp implements MovieDAO {
    private Connection connection;

    public MovieDAOImp(Connection connection) {
        this.connection = connection;
    }

    public void closeConn(){
        try {
            connection.close();
        } catch (SQLException e) {
        }
    }
    /**
     * @throws DatabaseActionException v pripade ze dojde k SQL Exception tak je prebalena do DatabaseActionException
     */
    @Override
    public void createTable() {
        String sql = """
                CREATE TABLE IF NOT EXISTS MOVIES(
                id INTEGER AUTO_INCREMENT,
                title VARCHAR(255),
                genre VARCHAR(255),
                year_of_release INTEGER,
                PRIMARY  KEY (id)) 
                """;
//        String createSql = "CREATE TABLE MOVIES(" +
//                "id INTEGER AUTO_INCREMENT, " +
//                "title VARCHAR(255), " +
//                "genre VARCHAR(255)," +
//                "year_of_release INTEGER," +
//                "PRIMARY  KEY (id)" +
//                ")";

        try (Statement statement = connection.createStatement()) {
            statement.execute(sql);
        } catch (SQLException e) {
            throw new DatabaseActionException("Exception in create table", e);
        }
    }

    @Override
    public void deleteTable() {
        String delete = "DROP TABLE MOVIES";
        try (Statement statement = connection.createStatement()) {
            statement.execute(delete);
        } catch (SQLException e) {
            throw new DatabaseActionException("Exception in create table", e);
        }
    }

    @Override
    public void createMovie(Movie movie) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(Movie.toCreateSQLString())) {
            preparedStatement.setString(1, movie.getTitle());
            preparedStatement.setString(2, movie.getGenre());
            preparedStatement.setInt(3, movie.getYearOfRelease());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DatabaseActionException("Exception in create table", e);
        }
    }


    @Override
    public void deleteMovie(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(Movie.toDeleteSQLString())) {
            preparedStatement.setInt(1, id);
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new RecordNotFoundException("MOVIES", id);
            }
        } catch (SQLException e) {
            throw new DatabaseActionException("Exception in create table", e);
        }
    }

    @Override
    public void updateMoviesTitle(int id, String newTitle) {
        String update = "UPDATE MOVIES SET TITLE = ? WHERE id = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(update)) {
            preparedStatement.setString(1, newTitle);
            preparedStatement.setInt(2, id);
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new RecordNotFoundException("MOVIES", id);
            }
        } catch (SQLException e) {
            throw new DatabaseActionException("Exception in create table", e);
        }
    }

    @Override
    public Optional<Movie> findMovieById(int id) {
        String sql = "SELECT * FROM MOVIES WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                Movie movie = new Movie(resultSet.getInt("id"),
                        resultSet.getString("title"),
                        resultSet.getString("genre"),
                        resultSet.getInt("year_of_release"));
                if(resultSet.next()){
                        throw new UnexceptedCountResult();
                }
                return Optional.of(movie);
            }else {
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new DatabaseActionException("Exception in create table", e);
        }
    }

    @Override
    public List<Movie> findAll() {
        String findMovie = "SELECT * FROM MOVIES";
        try (PreparedStatement preparedStatement = connection.prepareStatement(findMovie)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Movie> movies = new ArrayList<>();
            while (resultSet.next()) {
                Movie movie = new Movie(resultSet.getInt("id"),
                        resultSet.getString("title"),
                        resultSet.getString("genre"),
                        resultSet.getInt("year_of_release"));
                movies.add(movie);
            }
            return movies;
        } catch (SQLException e) {
            throw new DatabaseActionException("Exception in create table", e);
        }
    }
}
