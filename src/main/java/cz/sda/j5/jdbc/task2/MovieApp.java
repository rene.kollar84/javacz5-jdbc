package cz.sda.j5.jdbc.task2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class MovieApp {
    private String url = "jdbc:mysql://localhost:3306/jdbc";
    private String user = "root";
    private String password;
    private MovieDAO movieDAO;

    public void runApp(String[] args) throws SQLException {
        if (args.length > 0) {
            password = args[0];
        }
        Connection connection = createConnection();
        movieDAO = MovieDAO.createInstance(connection);
        startLoop();
        //  if(movieDAO instanceof MovieDAOImp mdImp){}

    }

    public void startLoop() {
        Scanner scanner = new Scanner(System.in);
        String line = null;

        do {
            try {


            /*
            exit
            createTable
            deleteTable
            createMovie  title genre year
            showAll
            printById id
            updateMovie id title
             */
                line = scanner.nextLine();
                boolean printAck = true;
                String[] arguments = parseArguments(line);
                switch (arguments[0]) {
                    case "switch":
                        switchDao(arguments[1]);
                    case "exit":
                        break;
                    case "createTable":
                        createTable();
                        break;
                    case "deleteTable":
                        deleteTable();
                        break;
                    case "createMovie":
                        createMovie(arguments[1], arguments[2], arguments[3]);
                        break;
                    case "showAll":
                        showAll();
                        break;
                    case "printById":
                        printById(arguments[1]);
                        break;
                    case "updateMovie":
                        updateMovie(arguments[1], arguments[2]);
                        break;
                    case "help":
                        System.out.println("""
                                            exit
                                            createTable
                                            deleteTable
                                            createMovie  title genre year
                                            showAll
                                            printById id
                                            updateMovie id title
                                            help
                                            switch SQL | Mem
                                """);
                        break;
                    default:
                        System.out.println("Prikaz neexistuje");
                        printAck = false;
                }
                if (printAck) {
                    System.out.println(arguments[0] + " executed.");
                }
            } catch (DatabaseActionException | SQLException e) {
                e.printStackTrace();
            }
        } while (!"exit".equals(line));
    }

    private static String[] parseArguments(String line) {
        StringBuffer buf = new StringBuffer();
        List<String> params = new ArrayList<>();
        int delNumb = 0;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '"') {
                delNumb++;
            }
            if (delNumb == 0 && line.charAt(i) == ' '){
                params.add(buf.toString());
                buf = new StringBuffer();
            }
        }
        return line.split(" ");
    }

    private void switchDao(String argument) throws SQLException {
        if (movieDAO instanceof MovieDAOImp sqlDao) {
            sqlDao.closeConn();
        }
        if ("SQL".equals(argument)) {
            movieDAO = MovieDAO.createInstance(createConnection());
        } else if ("Mem".equals(argument)) {
            movieDAO = MovieDAO.createInstance();
        }
        System.out.println("Now using " + movieDAO.getClass().getSimpleName());
    }

    private void updateMovie(String id, String title) {
        int intId = Integer.parseInt(id);
        movieDAO.updateMoviesTitle(intId, title);
    }

    private void printById(String argument) {
        int id = Integer.parseInt(argument);
        Optional<Movie> movieById = movieDAO.findMovieById(id);
        movieById.ifPresentOrElse(System.out::println, () -> System.out.println("Nenalezeno"));
    }

    private void showAll() {
        // movieDAO.findAll().forEach(movie -> System.out.println(movie));
        movieDAO.findAll().forEach(this::print);
    }

    public void print(Movie movie) {
        System.out.println(movie);
    }

    private void createMovie(String title, String genre, String yearOfRelease) {
        int year = Integer.parseInt(yearOfRelease);
        movieDAO.createMovie(new Movie(0, title, genre, year));
    }

    private void deleteTable() {
        movieDAO.deleteTable();
    }

    private void createTable() {
        movieDAO.createTable();
    }


    private Connection createConnection() throws SQLException {
        boolean continu = false;
        Connection connection = null;
        do {
            try {
                connection = DriverManager.getConnection(url, user, password);
                return connection;
            } catch (Throwable e) {
                continu = true;
                refreshPasword();
                if (password.equals("switch")) {
                    return null;
                }
            }
        } while (continu);
        return connection;
    }

    private void refreshPasword() {
        System.out.println("Try to entre valid password or type switch to Mem");
        Scanner scanner = new Scanner(System.in);
        password = scanner.nextLine();
    }

    public static void main(String[] args) {
        MovieApp movieApp = new MovieApp();
        try {
            movieApp.runApp(args);
        } catch (SQLException e) {
            System.out.println("Aplikace bola ukoncena " + e.getMessage());
        }
    }
}
