package cz.sda.j5.jdbc.task2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Movie {
    private int id;
    private String title;
    private String genre;
    private int yearOfRelease;

    public static String toDeleteSQLString() {
        return "DELETE FROM MOVIES WHERE id=?";
    }

    public static String toCreateSQLString() {
       return "INSERT INTO MOVIES(title,genre,year_of_release) VALUES (?,?,?)";
    }
}
