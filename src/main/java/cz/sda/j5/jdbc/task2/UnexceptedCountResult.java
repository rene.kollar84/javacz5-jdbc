package cz.sda.j5.jdbc.task2;

public class UnexceptedCountResult extends DatabaseActionException{
    public UnexceptedCountResult() {
        super("Unexcepted count of result ");
    }
}
