package cz.sda.j5.jdbc.task2;

public class DatabaseActionException extends RuntimeException{

    public DatabaseActionException(String message) {
        super(message);
    }

    public DatabaseActionException(String message, Throwable cause) {
        super(message, cause);
    }
}
