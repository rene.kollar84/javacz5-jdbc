package cz.sda.j5.jdbc.task2;

public class RecordNotFoundException extends DatabaseActionException{
    public RecordNotFoundException(String table,int id) {
        super(table + " with ID is not found with id: "+id);
    }
}
