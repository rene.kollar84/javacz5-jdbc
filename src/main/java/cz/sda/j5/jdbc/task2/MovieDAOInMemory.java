package cz.sda.j5.jdbc.task2;

import java.util.*;

public class MovieDAOInMemory implements MovieDAO{
    private List<Movie> movieList = new ArrayList<>();
    @Override
    public void createTable() {

    }

    @Override
    public void deleteTable() {

    }

    @Override
    public void createMovie(Movie movie) {
        movie.setId(movieList.size()+1);
        movieList.add(movie);
    }

    @Override
    public void deleteMovie(int id) {
        movieList.remove(id-1);
    }

    @Override
    public void updateMoviesTitle(int id, String newTitle) {
        movieList.get(id - 1).setTitle(newTitle);
    }

    @Override
    public Optional<Movie> findMovieById(int id) {
        return Optional.of(movieList.get(id-1));
    }

    @Override
    public List<Movie> findAll() {
        return Collections.unmodifiableList(movieList);
    }
}
