package cz.sda.j5.jdbc.task2;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public interface MovieDAO {
    void createTable();
    void deleteTable();

    void createMovie(final Movie movie);
    void deleteMovie(int id);
    void updateMoviesTitle(int id, String newTitle);
    Optional<Movie> findMovieById(int id);
    List<Movie> findAll();

    static MovieDAO createInstance(){
        return new MovieDAOInMemory();
    };
    static MovieDAO createInstance(Connection connection){
        return connection==null?createInstance():new MovieDAOImp(connection);

    }
}
