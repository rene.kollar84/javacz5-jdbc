package cz.sda.j5.jdbc.example1;

public class PersonIdException extends RuntimeException{
    public PersonIdException(String id) {
        super(  "Bad id:"+id);
    }
}
