package cz.sda.j5.jdbc.example1;

import java.sql.*;

/*
Task 1
Using the JDBC API and any relational database (e.g. H2) make the following queries:

create a table MOVIES with columns: id of type INTEGER AUTO INCREMENT,title of type VARCHAR (255), genre of type VARCHAR (255),
yearOfRelease of type INTEGER. Note that a table named MOVIE may already exist. In that case, delete it.
add any three records to the MOVIES table
update one selected record (use the PreparedStatement)
delete selected record with specified id
display all other records in the database
In the task, focus on the correct use of the JDBC API. All queries can be made directly in the main method. Use a single connection
to execute all queries. However, remember to use try-with-resources when opening a connection and creating objects such asStatement or PreparedStatement.
Also, don't worry about exception handling in this task (in case of error, display stacktrace).
*/
public class MainExample {

    private static String url = "jdbc:mysql://localhost:3306/jdbc";
    private static String user = "root";
    private static String password;

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Zadejte heslo v argumentu programu");
            return;
        }
        if (args.length == 2) {
            user = args[1];
        }
        password = args[0];
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            dropIfExists(connection);
            createTable(connection);
            insertMovie(connection, "Matrix", "Sci-Fi", 1999);
            insertMovie(connection, "Alien", "Sci-Fi", 1979);
            insertMovie(connection, "Predator", "Sci-Fi", 1987);
            deleteMovie(connection, "Alien");
            printOtherRecords(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createTable(Connection connection) throws SQLException {
        String createSql = "CREATE TABLE MOVIES(" +
                "id INTEGER AUTO_INCREMENT, " +
                "title VARCHAR(255), " +
                "genre VARCHAR(255)," +
                "year_of_release INTEGER," +
                "PRIMARY  KEY (id)" +
                ")";
        try (Statement statement = connection.createStatement()) {
            statement.execute(createSql);
        }
    }

    private static void dropIfExists(Connection connection) throws SQLException {
        String dropTableSql = "Drop table if exists movies";

        try (Statement statement = connection.createStatement()) {
            statement.execute(dropTableSql);
        }
    }

    private static void insertMovie(Connection connection, String title, String genre, Integer yearOfRelease) throws SQLException {
        String insertMovie = "INSERT INTO MOVIES (title, genre, year_of_release) VALUES (?,?,?)";

        try (PreparedStatement statement = connection.prepareStatement(insertMovie)) {
            statement.setString(1, title);
            statement.setString(2, genre);
            statement.setInt(3, yearOfRelease);
            statement.execute();
        }
    }

    private static void deleteMovie(Connection connection, String title) throws SQLException {
        String deleteMovie = "DELETE FROM MOVIES WHERE title = ?";

        try (PreparedStatement statement = connection.prepareStatement(deleteMovie)) {
            statement.setString(1, title);
            int i = statement.executeUpdate();
        }
    }

    private static void printOtherRecords(Connection connection) throws SQLException {
        String printOtherRecords = "SELECT * FROM MOVIES";

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(printOtherRecords);
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1) + " " + resultSet.getString(2) + " " + resultSet.getString(3));
            }
        }

    }
}
