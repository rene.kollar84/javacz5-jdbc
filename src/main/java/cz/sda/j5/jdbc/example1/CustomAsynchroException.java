package cz.sda.j5.jdbc.example1;

public class CustomAsynchroException extends RuntimeException{
    public CustomAsynchroException(String message) {
        super(message);
    }

    public CustomAsynchroException(String message, Throwable cause) {
        super(message, cause);
    }
}
