package cz.sda.j5.jdbc.example1;

public class CustumExceptionMain {
    public static void main(String[] args) throws CustumSynchroExecption {
        asynchroMethod();

        synchException();
    }

    private static void synchException() throws CustumSynchroExecption {
        //some code
        //.....
        //bad condition throwing exception
        throw new CustumSynchroExecption("Somthing went wrong");
    }

    private static void asynchroMethod(){
        //...
        throw new CustomAsynchroException("som. went wrong");
    }

    private static void re(){
        throw new RuntimeException("something");
    }
}
