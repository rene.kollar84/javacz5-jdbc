package cz.sda.j5.jdbc.example1;

import java.util.Date;

public class PersonIdMain {
    public static void main(String[] args) {
        testPID("12052811119");
        Date date = new Date();
    }

    private static void testPID(String s) {
        if(s.length()!=12){
            throw new PersonIdException(s);
        }
    }

}
