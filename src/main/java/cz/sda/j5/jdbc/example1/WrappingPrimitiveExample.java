package cz.sda.j5.jdbc.example1;

import java.util.ArrayList;
import java.util.List;

public class WrappingPrimitiveExample {
    static int lastTimeStamp;
    Integer lts = null;

    public static void main(String[] args) {
        int i = 10;
        int r = lastTimeStamp;
        Integer j = Integer.valueOf(3);
        Integer k = 2;
        Integer m = i;
        i = j;
        int[] arrI;
        //List<int> aa; not work generic must be Object type
        List<Integer> list = new ArrayList<>();
        list.add(3);
        int integer = list.get(0);

    }
}
