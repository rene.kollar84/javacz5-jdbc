package cz.sda.j5.jdbc.example1;

public class CustumSynchroExecption extends Exception{

    public CustumSynchroExecption(String message) {
        super(message);
    }

    public CustumSynchroExecption(String message, Throwable cause) {
        super(message, cause);
    }
}
