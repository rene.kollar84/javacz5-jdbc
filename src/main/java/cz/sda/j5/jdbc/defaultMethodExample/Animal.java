package cz.sda.j5.jdbc.defaultMethodExample;

public class Animal implements Infoable{
    @Override
    public String getInfo() {
        return "I am animal";
    }
}
