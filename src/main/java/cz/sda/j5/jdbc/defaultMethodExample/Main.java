package cz.sda.j5.jdbc.defaultMethodExample;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Infoable one = new Animal();
        Infoable two = new Human();
        System.out.println(one.getInfo());
        System.out.println(two.getInfo());

        one.printInfo();
        two.printInfo();

    }
}
