package cz.sda.j5.jdbc.defaultMethodExample;

public interface Infoable {
    String getInfo();

    default void printInfo(){
        System.out.println(getInfo());
    }
}
