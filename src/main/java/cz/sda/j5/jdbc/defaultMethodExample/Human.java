package cz.sda.j5.jdbc.defaultMethodExample;

public class Human implements Infoable{
    @Override
    public String getInfo() {
        return "I am human";
    }
}
