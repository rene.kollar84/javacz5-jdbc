package cz.sda.j5.jdbc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

public class ExeceptionExampleMain {
    public static void main(String[] args) throws Exception {
        try {
            a();
        }catch(Throwable t){
            t.printStackTrace();
        }
        System.out.println("2.");
        try {
            printFile("nonExsist.....");
        } catch (FileNotFoundException e) {
            throw new Exception(e);
        }
    }

    static void a() {
        b();
    }

    private static void b() {
        c();
    }

    private static void c() {
        int a = 5 / 0;
    }

    private static void printFile(String file) throws FileNotFoundException, UnknownHostException {
        if (new File(file).exists()) {
            //..do some print statements
        } else {
            throw new FileNotFoundException("Soubor " + file + "neexistuje");
        }
        throw new UnknownHostException();

    }


}
