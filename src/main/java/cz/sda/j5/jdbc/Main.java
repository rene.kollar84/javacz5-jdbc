package cz.sda.j5.jdbc;

import java.sql.*;

public class Main {

    private static String url = "jdbc:mysql://localhost:3306/jdbc";
    private static String user = "root";
    private static String password;

//    private static String url = "jdbc:h2:mem:testdb";
//    private static String user = "sa";
//    private static String password = "";

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Zadejte heslo v argumentu programu");
            return;
        }
        if (args.length == 2) {
            user = args[1];
        }
        password = args[0];
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            createTable(connection);
            insertData(connection);
            printTable(connection);
            deleteRedundant(connection);
            update(connection);
            printTable(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createTable(Connection conn) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS ANIMAL(" +
                "id INTEGER AUTO_INCREMENT, " +
                "name VARCHAR(255), " +
                "type VARCHAR(255)," +
                "PRIMARY  KEY (id)" +
                ")";
        try (Statement statement = conn.createStatement()) {
            statement.execute(sql);
        }

    }

    public static void insertData(Connection conn) throws SQLException {
        String sql = "INSERT INTO ANIMAL (name,type) VALUES (?,?)";
        try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
            preparedStatement.setString(1, "cat");
            preparedStatement.setString(2, "domestic");
            int i = preparedStatement.executeUpdate();
            System.out.println("Rows affected " + i);
        }
    }

    public static void printTable(Connection conn) throws SQLException {
        String sql = "SELECT * FROM ANIMAL WHERE type = ?";

        try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
            preparedStatement.setString(1, "domestic");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int rId = resultSet.getInt(1);
                String name = resultSet.getString("name");
                String type = resultSet.getString(3);
                System.out.println(String.format("%s %s %s", rId, name, type));
            }
        }
    }

    public static void deleteRedundant(Connection c) throws SQLException {
        String sql = "DELETE FROM ANIMAL WHERE id>1";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            int i = preparedStatement.executeUpdate();
            System.out.println(i + " rows deleted");
        }
    }

    public static void update(Connection c) throws SQLException {
        String sql = "UPDATE ANIMAL set name=?";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setString(1, "Kocka");
            int i = preparedStatement.executeUpdate();
            System.out.println(i + "rows updated");
        }
    }
}
